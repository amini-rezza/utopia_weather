part of 'search_page_bloc.dart';

abstract class SearchPageEvent extends Equatable {
  const SearchPageEvent();

  @override
  List<Object> get props => [];
}

class CityChanged extends SearchPageEvent {
  final String cityName;

  CityChanged({@required this.cityName});

  @override
  List<Object> get props => [cityName];
}

class SearchButtonPressed extends SearchPageEvent {}
