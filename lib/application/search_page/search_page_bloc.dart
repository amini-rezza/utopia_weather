import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:utopia_weather/data/weather_failure.dart';
import 'package:utopia_weather/data/weather_model.dart';
import 'package:utopia_weather/data/weather_repository.dart';
import 'package:utopia_weather/presentation/weather_page/weather_page.dart';

part 'search_page_event.dart';
part 'search_page_state.dart';

class SearchPageBloc extends Bloc<SearchPageEvent, SearchPageState> {
  final WeatherRepository weatherRepository;

  SearchPageBloc({@required this.weatherRepository})
      : super(SearchPageInitial());

  @override
  Stream<SearchPageState> mapEventToState(
    SearchPageEvent event,
  ) async* {
    if (event is CityChanged) {
      yield SearchInputUpdated(cityName: event.cityName);
    } else if (event is SearchButtonPressed) {
      final currentState = state as SearchInputUpdated;
      yield Loading();
      bool requestSuccessful = true;
      WeatherModel weatherModel;
      final response = await weatherRepository
          .getWeatherByCityName(currentState.cityName.trim());
      yield response.fold((l) {
        requestSuccessful = false;
        return SearchFailed(weatherFailure: l);
      }, (r) {
        weatherModel = r;
        return SearchSuccessful(weatherModel: r);
      });
      if (requestSuccessful) Get.to(WeatherPage(weatherModel: weatherModel));
    }
  }
}
