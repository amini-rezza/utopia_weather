part of 'search_page_bloc.dart';

abstract class SearchPageState extends Equatable {
  @override
  List<Object> get props => [];
}

class SearchPageInitial extends SearchPageState {}

class SearchInputUpdated extends SearchPageState {
  final String cityName;

  SearchInputUpdated({@required this.cityName});

  @override
  List<Object> get props => [cityName];
}

class SearchSuccessful extends SearchPageState {
  final WeatherModel weatherModel;

  SearchSuccessful({@required this.weatherModel});

  @override
  List<Object> get props => [weatherModel];
}

class SearchFailed extends SearchPageState {
  final WeatherFailure weatherFailure;

  SearchFailed({@required this.weatherFailure});

  @override
  List<Object> get props => [weatherFailure];
}

class Loading extends SearchPageState {}
