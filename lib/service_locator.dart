import 'package:get_it/get_it.dart';
import 'package:utopia_weather/application/search_page/search_page_bloc.dart';
import 'package:utopia_weather/data/weather_api.dart';
import 'package:utopia_weather/data/weather_repository.dart';

GetIt getIt = GetIt.instance;

setUpServiceLocator() {
  getIt.registerSingleton<WeatherApi>(WeatherApi());
  getIt.registerLazySingleton<WeatherRepository>(
      () => WeatherRepositoryImpl(weatherApi: getIt<WeatherApi>()));
  getIt.registerFactory<SearchPageBloc>(
      () => SearchPageBloc(weatherRepository: getIt<WeatherRepository>()));
}
