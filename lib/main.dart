import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_navigation/src/root/root_widget.dart';
import 'package:utopia_weather/application/search_page/search_page_bloc.dart';
import 'package:utopia_weather/presentation/search_page/search_page.dart';
import 'package:utopia_weather/service_locator.dart';

void main() {
  setUpServiceLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Utopia Weather',
      theme: ThemeData(
        primaryColor: Color(0xff427BFF),
        accentColor: Color(0xff0E33FF),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
          create: (context) => getIt<SearchPageBloc>(), child: SearchPage()),
    );
  }
}
