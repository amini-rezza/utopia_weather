import 'package:flutter/material.dart';

class WeatherAttribute extends StatelessWidget {
  final IconData iconData;
  final String name;
  final String value;

  WeatherAttribute(
      {@required this.iconData, @required this.name, @required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      height: 50,
      child: Row(
        children: [
          Icon(
            iconData,
            color: Colors.white,
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                name,
                style: TextStyle(
                    fontSize: 16, color: Colors.white.withOpacity(0.6)),
              ),
              Text(
                value,
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
