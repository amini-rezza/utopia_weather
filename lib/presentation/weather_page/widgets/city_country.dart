import 'package:flutter/material.dart';
import 'package:utopia_weather/data/weather_model.dart';

class CityAndCountry extends StatelessWidget {
  final WeatherModel weatherModel;

  CityAndCountry({@required this.weatherModel});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          '${weatherModel.country}, ',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
        ),
        Text(
          weatherModel.city,
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
        )
      ],
    );
  }
}
