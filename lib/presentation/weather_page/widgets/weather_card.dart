import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:utopia_weather/data/weather_model.dart';
import 'package:utopia_weather/presentation/weather_page/widgets/weather_attribute.dart';

class WeatherCard extends StatelessWidget {
  final WeatherModel weatherModel;

  WeatherCard({@required this.weatherModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 6 * 4,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              _mapDescriptionToAssetAddres(weatherModel.description),
              width: 90,
              height: 90,
            ),
            Text(
              weatherModel.description,
              style: TextStyle(
                color: Colors.white,
                fontSize: 35,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              DateFormat.MMMd().format(DateTime.now()),
              style:
                  TextStyle(color: Colors.white.withOpacity(0.6), fontSize: 18),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              '${weatherModel.tempeture}\u00B0',
              style: TextStyle(color: Colors.white, fontSize: 80),
            ),
            const SizedBox(
              height: 20,
            ),
            Divider(
              height: 10,
              color: Colors.white,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      WeatherAttribute(
                          iconData: CommunityMaterialIcons.weather_windy,
                          name: 'WIND',
                          value: '${weatherModel.windSpeed} km/h'),
                      WeatherAttribute(
                        iconData: CommunityMaterialIcons.coolant_temperature,
                        name: 'FEELS LIKE',
                        value: '${weatherModel.feelsLike}\u00B0',
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      WeatherAttribute(
                        iconData: CommunityMaterialIcons.water,
                        name: 'HUMIDIDTY',
                        value: '${weatherModel.humidity}%',
                      ),
                      WeatherAttribute(
                        iconData: Icons.waves,
                        name: 'PRESSURE',
                        value: '${weatherModel.pressure} mbar',
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  _mapDescriptionToAssetAddres(String description) {
    String assetAddres;
    switch (description) {
      case 'clear sky':
        assetAddres = 'assets/sunny.svg';
        break;
      case 'few clouds':
        assetAddres = 'assets/cloud.svg';
        break;
      case 'scattered clouds':
        assetAddres = 'assets/cloudy.svg';
        break;
      case 'shower rain':
        assetAddres = 'assets/storm.svg';
        break;
      case 'rain':
        assetAddres = 'assets/thunder.svg';
        break;
      case 'snow':
        assetAddres = 'assets/snow.svg';
        break;
      default:
        assetAddres = 'assets/sunny.svg';
    }
    return assetAddres;
  }
}
