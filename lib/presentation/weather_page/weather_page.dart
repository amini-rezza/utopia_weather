import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:utopia_weather/data/weather_model.dart';
import 'package:utopia_weather/presentation/weather_page/widgets/city_country.dart';
import 'package:utopia_weather/presentation/weather_page/widgets/weather_card.dart';

class WeatherPage extends StatefulWidget {
  final WeatherModel weatherModel;

  WeatherPage({@required this.weatherModel});

  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(CommunityMaterialIcons.arrow_left),
          onPressed: () {
            Get.back();
          },
          color: Colors.black,
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Column(
            children: [
              CityAndCountry(weatherModel: widget.weatherModel),
              const SizedBox(
                height: 30,
              ),
              WeatherCard(weatherModel: widget.weatherModel),
            ],
          ),
        ),
      ),
    );
  }
}
