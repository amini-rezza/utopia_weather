import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:utopia_weather/application/search_page/search_page_bloc.dart';
import 'package:utopia_weather/presentation/search_page/widgets/search_button.dart';
import 'package:utopia_weather/presentation/search_page/widgets/search_input.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SearchPageBloc, SearchPageState>(
      listener: (context, state) {
        if (state is SearchFailed)
          Get.snackbar('Sorry', state.weatherFailure.failureMessage);
      },
      builder: (context, state) => ModalProgressHUD(
        inAsyncCall: state is Loading,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 45),
              SearchInput(),
              const SizedBox(height: 25),
              SearchButton(),
            ],
          ),
        ),
      ),
    );
  }
}
