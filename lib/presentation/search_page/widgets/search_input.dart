import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:utopia_weather/application/search_page/search_page_bloc.dart';

class SearchInput extends StatefulWidget {
  @override
  _SearchInputState createState() => _SearchInputState();
}

class _SearchInputState extends State<SearchInput> {
  TextEditingController textEditingController;
  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final SearchPageBloc bloc = BlocProvider.of<SearchPageBloc>(context);
    return BlocBuilder<SearchPageBloc, SearchPageState>(
      builder: (context, state) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: TextField(
          controller: textEditingController,
          onChanged: (newValue) {
            bloc.add(CityChanged(cityName: textEditingController.text));
          },
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(60),
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 3.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(60),
              borderSide:
                  BorderSide(color: Theme.of(context).accentColor, width: 1.5),
            ),
            prefixIcon: Icon(Icons.search),
            hintStyle: TextStyle(color: Colors.grey[500]),
            hintText: "Enter Your City",
          ),
        ),
      ),
    );
  }
}
