import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:utopia_weather/application/search_page/search_page_bloc.dart';

class SearchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final SearchPageBloc bloc = BlocProvider.of<SearchPageBloc>(context);
    return FlatButton(
      onPressed: () {
        FocusScope.of(context).unfocus();

        bloc.add(SearchButtonPressed());
      },
      color: Theme.of(context).accentColor,
      height: 50,
      minWidth: 100,
      shape: RoundedRectangleBorder(
        side: BorderSide.none,
        borderRadius: BorderRadius.circular(50),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
        child: Text(
          'Search',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }
}
