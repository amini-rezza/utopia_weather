import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:utopia_weather/data/weather_api.dart';
import 'package:utopia_weather/data/weather_failure.dart';
import 'package:utopia_weather/data/weather_model.dart';

abstract class WeatherRepository {
  Future<Either<WeatherFailure, WeatherModel>> getWeatherByCityName(
      String cityName);
}

class WeatherRepositoryImpl extends WeatherRepository {
  final WeatherApi weatherApi;

  WeatherRepositoryImpl({@required this.weatherApi});

  @override
  Future<Either<WeatherFailure, WeatherModel>> getWeatherByCityName(
      String cityName) async {
    try {
      Response response = await weatherApi.getWeatherByCityName(cityName);
      return right(WeatherModel.fromJson(response));
    } on CityNotFoundException {
      return left(WeatherFailure(
          failureMessage:
              'Could not find Your city. make sure you write it correctly and then try again'));
    } on NoConnectionException {
      return left(WeatherFailure(
          failureMessage: 'Your device is not connected to newtwork'));
    } on UnexpectedException {
      return left(WeatherFailure(
          failureMessage:
              'Something horribly went Wrong ,contact our support for resolving issue or getting more information'));
    }
  }
}
