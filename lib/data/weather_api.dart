import 'dart:io';

import 'package:dio/dio.dart';
import 'package:utopia_weather/data/weather_failure.dart';

class WeatherApi {
  static const apiKey = '94043d3541ce75fa6dccd3989913133f';
  static const String baseUrl = 'api.openweathermap.org';

  Future<Response> getWeatherByCityName(String city) async {
    final String path = 'data/2.5/weather';
    final queryParameters = {'q': '$city', 'appid': apiKey, 'units': 'metric'};
    final url = Uri.https(baseUrl, path, queryParameters);
    try {
      Response response = await Dio().get(url.toString());
      return response;
    } on DioError catch (e) {
      if (e.error == 'Http status error [404]') throw CityNotFoundException();
      if (e.error.runtimeType == SocketException)
        throw NoConnectionException();
      else
        throw UnexpectedException();
    }
  }
}
