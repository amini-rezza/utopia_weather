abstract class WeatherException implements Exception {}

class CityNotFoundException extends WeatherException {}

class NoConnectionException extends WeatherException {}

class UnexpectedException extends WeatherException {}

class WeatherFailure {
  final String failureMessage;

  WeatherFailure({this.failureMessage});
}
