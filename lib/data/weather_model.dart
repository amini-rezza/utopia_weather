import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class WeatherModel extends Equatable {
  final String country;
  final String city;
  final String description;
  final String tempeture;
  final String windSpeed;
  final String feelsLike;
  final String humidity;
  final String pressure;

  WeatherModel(
      {@required this.country,
      @required this.city,
      @required this.description,
      @required this.tempeture,
      @required this.windSpeed,
      @required this.feelsLike,
      @required this.humidity,
      @required this.pressure});

  @override
  List<Object> get props => [
        country,
        city,
        description,
        tempeture,
        windSpeed,
        feelsLike,
        humidity,
        pressure
      ];

  @override
  bool get stringify => true;

  factory WeatherModel.fromJson(Response response) => WeatherModel(
        country: response.data['sys']['country'],
        city: response.data['name'],
        description: response.data['weather'][0]['description'],
        tempeture: response.data['main']['temp'].toString(),
        windSpeed: response.data['wind']['speed'].toString(),
        feelsLike: response.data['main']['feels_like'].toString(),
        humidity: response.data['main']['humidity'].toString(),
        pressure: response.data['main']['pressure'].toString(),
      );
}
